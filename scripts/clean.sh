#!/bin/bash

# This script cleans up old build files
rm -rf backend/static/
rm -rf frontend/dist/