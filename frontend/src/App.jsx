import { useState} from 'react'
import {v4 as uuidv4} from 'uuid';
import './App.css'
import axios from 'axios';


function App() {
    const [todos, setTodos] = useState([]);

    const getTodos = async () => {
        axios.get('http://localhost:80/api/todos')
            .then(res => {
                console.log(res.data)
                setTodos(res.data)
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <>
            <h1> Click the button to load the todos</h1>
            <h4>This sends a get request to the backend node.js api at http://localhost:80/api/todos</h4>
            <h4>This will return an array of todos that are mapped the DOM</h4>
            <button onClick={getTodos}>Get Todos</button>
            <h1>
                Todos:
            </h1>
            {todos.length > 0 ? todos.map(todo => (
                <div key={uuidv4()}>
                    <h2>{todo.text}</h2>
                    <p>{todo.completed}</p>
                </div>
            )) : null}


        </>
    )
}

export default App
